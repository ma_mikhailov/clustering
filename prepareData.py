import re

words = {}
stories = {}
stories_representation = {}
currentWdIdx = 0;

#Добавляем слово в глобальный словарь
def add_words(line):
  global currentWdIdx
  
  list = re.split(r'\s', line.upper())
  for word in list:
    if not word: continue
    if len(word) <=2: continue
    
    if not words.get(word):
      currentWdIdx += 1
      words[word] = currentWdIdx

#представляем story в виде хеша где key = words_index value = number_of_words_in_a_story      
def representStory(key, story_lines):

  rep = {}
  for story in story_lines:
    list = re.split(r'\s', story.upper())
    for word in list:    
      
      if len(word) <=2: continue
      if not word: continue
      
      global_key = words[word]
      if rep.get(global_key):
        rep[global_key] += 1
      else:
        rep[global_key] = 1
      
  stories_representation[key] = rep

with open('data.txt') as f:

   while True:
      
    line = f.readline()
    if not line: break
    
    match = re.search(r'#story: (\d+)', line)
    if match:
      story_id = match.group(1)
      
      line = f.readline()
      buffer = []
      while not re.search(r'^#story_end', line):  
        line = re.sub(r'[\?!\:\(\)\.,]', '', line)
        add_words(line)
        buffer.append(line)
        line = f.readline()
        
      stories[story_id] = buffer
          

for key in stories:
  representStory(key, stories[key])

f = open('data_prep.txt', 'w')

# формируем текстовый файл для обработки алгоритмом K-means
# строка будет представлена в виде story_id, number_of_words_with_word_idx1, number_of_words_with_word_idx2, ...
for key in sorted(stories_representation):
  line = [str(key)] #первое значение - порядковый номер story
  rep = stories_representation[key]
  for k in range(1, len(words) + 1):
    if rep.get(k):
      line.append(str(rep.get(k)))
    else:
      line.append('0')
  
  print key
  f.write(','.join(line) + "\n")
  
f.close()

print currentWdIdx, len(words), len(stories_representation)