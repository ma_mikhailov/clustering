function idx = findClosestCentroids(X, centroids)

K = size(centroids, 1); %количество центроидов
m = size(X, 1);

idx = zeros(m, 1); %соотнесение центроидов с номером story

t = zeros(K, 1);

for i=1:m
  t = sum((centroids - X(i,:)) .^ 2, 2);
  [mx, idx(i)] = min(t);
end

% =============================================================

end

