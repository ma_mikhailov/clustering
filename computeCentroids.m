function centroids = computeCentroids(X, idx, K)

[m n] = size(X);

centroids = zeros(K, n);

for i=1:K
  
  idx1 = (idx == i);
  idxSum = sum(idx1); %сколько всего stories для этого центроида
  
  if idxSum == 0 
    continue
  end
  
  T = X .* idx1;
  centroids(i, :) = mean(T) .* m ./sum(idx1);
  
end


end

