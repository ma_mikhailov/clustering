﻿printf('\nLoad data.\n\n');
D = load('data_prep.txt');

D = sortrows(D);

[m n] = size(D);

fprintf('\nK-Means clustering of bash.org data (%i stories)\n\n', m);

stories = D(:, 1);
X = D(:, 2:n);

K = 10; 
max_iters = 20;

fprintf('\nSet %i random centroids.\n\n', K);
initial_centroids = zeros(K, n - 1);

%take some randpom stories
[junk, R] = sort(rand(1,1000)); 
R = R(:,1:K); %need K different randoms indexes from 1 to 1000

for i=1:K;
  initial_centroids(i, :) = X(R(i), :);
end

fprintf('\nK-Means running.\n\n');
[centroids, idx] = runkMeans(X, initial_centroids, max_iters, true);

fprintf('\nidx contains %i values, corresponding to some of %i centroids.\n\n', m, K);

save 'results.txt' idx;

hist(idx,unique(idx));