function [centroids, idx] = runkMeans(X, initial_centroids, max_iters)

[m n] = size(X);
K = size(initial_centroids, 1);
centroids = initial_centroids;
idx = zeros(m, 1);

% Run K-Means
for i=1:max_iters
    
    % Output progress
    fprintf('K-Means iteration %d/%d...\n', i, max_iters);
    if exist('OCTAVE_VERSION')
        fflush(stdout);
    end
    
    idx = findClosestCentroids(X, centroids); 
    
    centroids = computeCentroids(X, idx, K);
end

end

