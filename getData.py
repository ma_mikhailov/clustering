import urllib2
from HTMLParser import HTMLParser
import re

links = []
 
class MyHTMLParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.inStory = False

    def handle_starttag(self, tag, attrs):
    
        self.inStory = False
    
        if tag != 'p':
            return
        attr = dict(attrs)
        if attr.get('class') == 'qt':
          self.total += 1
          self.inStory = True
          links.append("#story: " + str(self.total))
          
    def handle_data(self, data):
        if self.inStory and len(data) :
          links.append(data)
          
    def handle_endtag(self, tag):
        if self.inStory and tag == 'p':
          links.append("#story_end")
          
def extract():
    
    page = 0
    parser = MyHTMLParser()
    parser.total = 0
    f = open('data.txt', 'w')
    
    while parser.total < 1000 :
    
      page += 1
      url = ' http://bash.org/?browse&p=' + str(page)
   
      try:
          h = urllib2.urlopen(url)
          html = h.read().replace('<br />', ' ')
          html = re.sub(r'&lt;[^<>&]*&gt;', '', html)
          h.close()
      except urllib2.HTTPError as e:
          print(e, 'while fetching', url)
          return
     
      parser.feed(html)
      
      for l in links:
        f.write(l)
        f.write("\n")
        
    f.close()
    print 'Done!'
 
extract()

